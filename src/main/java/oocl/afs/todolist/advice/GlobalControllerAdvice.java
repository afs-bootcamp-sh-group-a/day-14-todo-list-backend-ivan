package oocl.afs.todolist.advice;

import oocl.afs.todolist.service.except.NotFoundById;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalControllerAdvice {
    @ExceptionHandler(Exception.class)
    public ErrorResponse finalHandler(Exception e) {
        String message = String.format("出了点问题，请稍后重试: %s", e.getMessage());
        return new ErrorResponse(501,message);
    }

    @ExceptionHandler(NotFoundById.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public ErrorResponse finalHandler(NotFoundById e) {
        return new ErrorResponse(502,e.getMessage());
    }
}
