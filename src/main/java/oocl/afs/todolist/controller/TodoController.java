package oocl.afs.todolist.controller;

import oocl.afs.todolist.service.TodoService;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    List<TodoResponse> getAll() {
        return todoService.findAll();
    }

    @PostMapping()
    TodoResponse create(@RequestBody TodoCreateRequest todoCreateRequest) {
        todoService.create(TodoMapper.toEntity(todoCreateRequest));
        return null;
    }

    @PutMapping("/{id}")
    TodoResponse update(@PathVariable("id") Integer id, @RequestBody TodoCreateRequest todoCreateRequest) {
        todoService.update(TodoMapper.toEntityById(id, todoCreateRequest));
        return null;
    }

    @DeleteMapping("/{id}")
    boolean delete(@PathVariable("id") Integer id) {
        todoService.delete(id);
        return true;
    }

}
