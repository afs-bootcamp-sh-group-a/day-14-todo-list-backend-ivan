package oocl.afs.todolist.service;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.except.NotFoundById;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> findAll() {
        return todoRepository.findAll()
                .stream()
                .map(TodoMapper::toResponse)
                .collect(Collectors.toList());
    }

    public void create(Todo todo) {
        todoRepository.save(todo);
    }

    public void delete(Integer id) {
        if (todoRepository.findById(id.longValue()).isEmpty())
            throw new NotFoundById();
        todoRepository.deleteById(id.longValue());
    }

    public void update(Todo todo) {
        if (todoRepository.findById(todo.getId()).isEmpty())
            throw new NotFoundById();
        todoRepository.save(todo);
    }
}
