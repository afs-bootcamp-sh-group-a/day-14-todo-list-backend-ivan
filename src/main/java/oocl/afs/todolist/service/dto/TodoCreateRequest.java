package oocl.afs.todolist.service.dto;

public class TodoCreateRequest {
    private String name;
    private boolean done;

    public TodoCreateRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
