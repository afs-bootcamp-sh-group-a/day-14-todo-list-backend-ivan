package oocl.afs.todolist.service.except;

public class NotFoundById extends RuntimeException{

    public static final String ID_NOT_BE_FOUND = "Id not be found";

    public NotFoundById() {
        super(ID_NOT_BE_FOUND);
    }
}
