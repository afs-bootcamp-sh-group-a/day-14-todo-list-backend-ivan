package oocl.afs.todolist;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TodoRepository todoRepository;

    @BeforeEach
    void setUp() {
        todoRepository.deleteAll();
    }

    @Test
    void should_find_todos() throws Exception {
        Todo todo = new Todo();
        todo.setName("Study React");
        todoRepository.save(todo);

        mockMvc.perform(get("/todo"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(todo.getName()));
    }

    @Test
    void should_delete_todo_when_delete() throws Exception {
        Todo todoA = new Todo();
        todoA.setName("Study React");
        Todo todoB = todoRepository.save(todoA);

        mockMvc.perform(delete("/todo/" + todoB.getId()))
                .andExpect(status().isOk());
        boolean success = todoRepository.findAll().stream()
                .allMatch(todo -> todo.getId().equals(todoB.getId()));
        assertTrue(success);
    }

    @Test
    void should_add_todo_when_post() throws Exception {
        String json = "{\"name\":\"ok\",\"done\":false}";
        mockMvc.perform(post("/todo").content(json).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        boolean success = todoRepository.findAll().stream()
                .allMatch(todo -> todo.getName().equals("ok"));
        assertTrue(success);
    }

    @Test
    void should_update_todo_when_put() throws Exception {
        Todo todoA = new Todo();
        todoA.setDone(false);
        todoA.setName("ok");
        Todo newTodo = todoRepository.save(todoA);
        String json = "{\"name\":\"nok\"}";
        mockMvc.perform(put("/todo/" + newTodo.getId()).content(json).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        boolean success = todoRepository.findAll().stream()
                .allMatch(todo -> todo.getName().equals("nok"));
        assertTrue(success);

        String jsonDone = "{\"done\":\"true\"}";
        mockMvc.perform(put("/todo/" + newTodo.getId()).content(jsonDone).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        boolean successDone = todoRepository.findAll().stream()
                .allMatch(todo -> todo.getDone().equals(true));
        assertTrue(successDone);

    }

    @Test
    void should_exception_when_create() throws Exception {

        boolean sucess = mockMvc.perform(delete("/todo/" + 111))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString().contains("Id not be found");
        assertTrue(sucess);
    }
}
